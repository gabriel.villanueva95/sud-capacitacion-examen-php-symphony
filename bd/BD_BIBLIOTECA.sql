/*
Examen Capacitacion PHP-Symfony
Autor: Gabriel Villanueva
*/

#Creación de la Base de Datos
CREATE DATABASE IF NOT EXISTS BIBLIOTECA;

#Seteado de la base de datos como la base de datos por defecto
USE BIBLIOTECA;

#DDL
#Creación de Tabla Libros
CREATE TABLE IF NOT EXISTS LIBRO(
ID					INT				PRIMARY KEY		auto_increment  COMMENT'Codigo unico del libro',
TITULO				VARCHAR(255)	NOT NULL						COMMENT 'Titulo del libro',
DESCRIPCION			VARCHAR(255)									COMMENT 'Descripcion breve del libro',
ANIO				INT 			NOT NULL						COMMENT 'Anio de publicacion',
AUTOR				VARCHAR(255)	NOT NULL						COMMENT 'Autor del libro'
);


#DML
INSERT INTO LIBRO(ID, TITULO, DESCRIPCION, ANIO, AUTOR) VALUES
(1, 'The Eye of the World', 'Moiraine, miembro de una organización magica, lleva a cinco jovenes en un viaje donde espera encontrar a quien es la reencarnacion del dragon.', 1990, 'Robert Jordan'),
(2, 'If I Stay', 'La historia sigue a Mia Hall, de 17 años, mientras lidia con las consecuencias de un catastrofico accidente automovilistico que involucra a su familia. Mia es el unico miembro de su familia que sobrevive y se encuentra en coma.', 2009, 'Gayle Forman'),
(3, 'Orgullo y Prejuicio', 'Orgullo y prejuicio narra las aventuras y desventuras amorosas de las hermanas Bennet, centrandose en el personaje de Elizabeth, a traves de las cuales la autora nos presenta con comicidad la sociedad de su tiempo.', 1813, 'Jane Austen');
