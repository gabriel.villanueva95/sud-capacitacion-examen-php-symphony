<?php

namespace App\Controller;

use App\Entity\Libro;
use App\Form\LibroFormType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CrearLibroController extends AbstractController
{
    /**
     * @Route("/crear/libro", name="app_crear_libro")
     */
    public function index(Request $req, ManagerRegistry $mr): Response
    {
        $formulario = $this->createForm(LibroFormType::class, new Libro());
        $formulario->handleRequest($req);

        if($formulario->isSubmitted() && $formulario->isValid()){
            $libro = $formulario->getData();
            $em = $mr->getManager();
            $em->persist($libro);
            $em->flush();
            return $this->redirectToRoute('app_main');
        }

        return $this->render('crear_libro/index.html.twig', [
            'formulario' => $formulario->createView(),
            'title' => 'Registrar Libro',
            'titleform' => 'Nuevo Libro',
        ]);
    }
}
