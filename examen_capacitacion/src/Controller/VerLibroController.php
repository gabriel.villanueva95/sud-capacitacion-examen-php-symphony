<?php

namespace App\Controller;

use App\Repository\LibroRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class VerLibroController extends AbstractController
{
    /**
     * @Route("/ver/libro/{id}", name="app_ver_libro")
     */
    public function index(LibroRepository $librorepo, $id): Response
    {
        return $this->render('ver_libro/index.html.twig', [
            'libro' => $librorepo->find($id),
        ]);
    }
}
