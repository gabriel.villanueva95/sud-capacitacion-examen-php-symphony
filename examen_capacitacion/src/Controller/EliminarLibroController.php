<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use App\Entity\Libro;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\RedirectResponse;

class EliminarLibroController extends AbstractController
{
    /**
     * @Route("/eliminar/libro/{id}", name="app_eliminar_libro")
     */
    public function index(Libro $plibro, ManagerRegistry $mr): RedirectResponse
    {
        $em = $mr->getManager();
        $em->remove($plibro);
        $em->flush();
        return $this->redirectToRoute('app_main');
    }
}
