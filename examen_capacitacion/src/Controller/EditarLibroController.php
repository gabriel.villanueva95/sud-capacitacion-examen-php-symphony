<?php

namespace App\Controller;

use App\Entity\Libro;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Doctrine\Persistence\ManagerRegistry;
use App\Form\LibroFormType;

class EditarLibroController extends AbstractController
{
    /**
     * @Route("/editar/libro/{id}", name="app_editar_libro")
     */
    public function index(Libro $plibro, Request $req, ManagerRegistry $mr): Response
    {
        $formulario = $this->createForm(LibroFormType::class, $plibro);
        $formulario->handleRequest($req);

        if($formulario->isSubmitted() && $formulario->isValid()){
            $libro = $formulario->getData();
            $em = $mr->getManager();
            $em->persist($libro);
            $em->flush();
            return $this->redirectToRoute('app_main');
        }

        return $this->render('crear_libro/index.html.twig', [
            'formulario' => $formulario->createView(),
            'title' => 'Editar Libro',
            'titleform' => 'Editar Libro',
        ]);
    }
}
